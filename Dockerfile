FROM mcr.microsoft.com/dotnet/runtime:6.0
USER root
RUN apt-get update
RUN apt-get -y install curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash -
RUN apt-get -y install git jq nodejs openssh-client unzip

RUN \
    id='microsoft.powerapps.cli.core.linux-x64' && \
    version=$(curl -s \
        "https://api.nuget.org/v3/registration5-semver1/$id/index.json" | \
        jq -r '.items[0].upper') && \
    curl -s \
        "https://api.nuget.org/v3-flatcontainer/$id/$version/$id.$version.nupkg" \
        -o "/tmp/$id.nupkg" && \
    unzip "/tmp/$id.nupkg" -d "/tmp/$id" && \
    mv "/tmp/$id/tools" '/opt/pac' && \
    chmod +x '/opt/pac/pac'
ENV PATH /opt/pac:$PATH
RUN pac telemetry disable
